
# Trabajo Practico Nro. 2
 

# Introducción
En este ejercicio, el objetivo es que al ejecutar el script se quede el proceso padre a la espera de las señales **SIGUSR1**, **SIGUSR2** y **SIGTERM**, donde cada una realiza las siguientes acciones:

 - **SIGUSR1**: Crea un proceso hijo el cual queda un bucle infinito imprimiendo su PID y el de su proceso padre cada 5 segundos.
 - **SIGUSR2**: Crea un proceso hijo el cual ejecuta un comando a traves de `exec()`. Luego de esta ejecucion, el proceso finaliza.
 - **SIGTERM**: Si existe algun proceso hijo creado con *SIGUSR1*, entonces cierra todos los que existan. Luego el proceso padre finaliza. 

  
  

# Modo de Uso

El Script fue escrito en lenguaje C por tanto debera primero compilarse:

    gcc -o Ejecutable Ejercicio6.c

Luego, para ejecutar el archivo creado, debera hacerse:

    ./Ejecutable

Finalmente, una vez ejecutado el proceso padre mostrara por unica vez el PID que tiene, por ejemplo: **8750**. Para enviarle las señales correspondientes, debera usarse otra terminal y hacer uso del comando `kill` en conjunto con el PID 8750 ya mencionado.

    kill -SIGUSR1 8750

Para poder visualizar el arbol de procesos, se puede utilizar el comando: `ps -uf`
