#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <stdlib.h>



#define MAX_HIJOS 100
pid_t pid_hijos[MAX_HIJOS];
int count_hijos = 0;
int flag1=0,flag2=0,flag3=0;

void crearHijo1(int sig){
    flag1=1;
}


void crearHijo2(int sig){
    flag2 = 1;
}


void terminarHijos(int sig){
    flag3 = 1;
}



int main() {
    
    signal(SIGUSR1, crearHijo1);
    signal(SIGUSR2, crearHijo2);
    signal(SIGTERM, terminarHijos);
    int pidp = getpid();
    
    printf("=> [Este es el PID %d del PADRE]: \n Abra una nueva terminal, use este PID: %d y envie alguna de las siguientes señales:\n", getpid(),getpid());
    printf("     SIGUSR1: Crea hijos que imprimen sus PID y el del padre cada 5 segundos.\n");
    printf("     SIGUSR2: Crea un hijo, ejecuta un [ls -la] y muere.\n");
    printf("     SIGTERM: Termina todos los hijos (si existe alguno) y luego finaliza el padre.\n");
    printf("Esperando señales:\n");


    while(1){
        if(pidp == getpid()){


            /* Hijos en Bucles */
            if(flag1==1){
                pid_t pid = fork();

                if(pid == 0) { //Hijo
                    while(1){
                        printf("===> [HIJO] : %d | [PADRE] : %d\n", getpid(), pidp);
                        sleep(5);
                    }
                } else if(pid > 0){ //Padre
                    pid_hijos[count_hijos] = pid;
                    count_hijos++;
                }
                flag1=0;
            }


            /* Hijo ejecuta y muere */
            if (flag2 == 1){
                pid_t pid = fork();

                if(pid == 0) { //Hijo
                    printf("[%d]: Ejecuto ls -la\n", getpid());
                    execlp("ls", "ls", "-la", NULL);
                    exit(0);
                } else if (pid > 0){ 
                    flag2 = 0;

                    int status;
                    waitpid(pid, &status, 0);

                }
            }


            /* Mata todos los hijos cada 1 seg */
            if(flag3 == 1 ){
                printf("Procedo a cerrar todos los hijos...\n");
                if(count_hijos > 0){
                    for(int i = 0; i < count_hijos; i++){

                        kill(pid_hijos[i], SIGKILL);

                        int status;
                        int pid = waitpid(pid_hijos[i], &status, 0);
                        if(pid != pid_hijos[i]){
                            printf("[ERROR]: No se pudo cerrar el proceso %d\n", pid_hijos[i]);
                        } else {
                            if(WIFSIGNALED(status)){
                                printf("===> [HIJO]:[%d] Finalizado con el estado [%d]...\n", pid_hijos[i], WEXITSTATUS(status)); 
                            }
                        }

                        sleep(1);
                    }
                    
                    //count_hijos = 0;
                }
                exit(0);
            }

        }
        
        
    }


    return 1;
}